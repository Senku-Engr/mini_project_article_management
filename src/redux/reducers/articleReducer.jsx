import * as actions from "../actions/actionType";

const defaultState = {
  data: [],
  articleById: null,
};

export const articleReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actions.GET_ARTICLE:
      return {
        ...state,
        data: action.data.data,
      };
    case actions.GET_ARTICLE_BY_ID:
      return {
        ...state,
        articleById: action.data.data,
      };

    case actions.POST_ARTICLE:
      return {
        ...state,
        data: [...state.data, action.data],
      };
    case actions.PUT_ARTICLE:
      return {
        ...state,
        data: state.data.map((d) => (d._id !== action.id ? d : action.data)),
      };
    case actions.DELETE_ARTICLE:
      return {
        ...state,
        data: state.data.filter((d) => d._id !== action.id),
      };
    case actions.SEARCH_ARTICLE_NO_AWAIT:
      return { ...state, data: action.data };
    case actions.GET_MORE_ARTICLES_NO_AWAIT:
      return { ...state, data: [...state.data, ...action.data.data] };
    default:
      return state;
  }
};
