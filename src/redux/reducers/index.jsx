import { combineReducers } from "redux";
import { articleReducer } from "./articleReducer";
import { categoryReducer } from "./categoryReducer";
const reducers = {
  articleReducer: articleReducer,
  categoryReducer: categoryReducer,
};
export const rootReducer = combineReducers(reducers);
