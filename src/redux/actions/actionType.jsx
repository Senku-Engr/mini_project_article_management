export const GET_ARTICLE = "get_article";
export const POST_ARTICLE = "post_article";
export const GET_ARTICLE_BY_ID = "get_article_by_id";
export const PUT_ARTICLE = "put_article";
export const DELETE_ARTICLE = "delete_article";
export const GET_ARTICLE_BY_ID_NO_AWAIT = "get_article_1_no_await";
export const SEARCH_ARTICLE_NO_AWAIT = "search_Article_no_await";
export const GET_MORE_ARTICLES_NO_AWAIT = "get_more_articles_no_await";

export const GET_CATEGORY = "get_category";
export const POST_CATEGORY = "post_category";
export const PUT_CATEGORY = "put_category";
export const DELETE_CATEGORY = "delete_categoey";

export const UPLOAD_PICTURE = "upload_picture";
