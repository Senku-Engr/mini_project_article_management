import { BASE_API } from "../../api/BaseApi";
import axios from "axios";
import * as actions from "../actions/actionType";

export const getCategory = () => {
  return (dp) => {
    axios.get(`${BASE_API}/category`).then((res) => {
      dp({
        type: actions.GET_CATEGORY,
        data: res.data.data,
      });
    });
  };
};

export const postCategory = (category, callBack) => {
  axios.post(`${BASE_API}/category`, category).then((res) => {
    callBack(res);
  });
};

export const deleteCategory = (id) => {
  return (dp) => {
    axios.delete(`${BASE_API}/category/${id}`).then((res) => {
      dp({
        type: actions.DELETE_CATEGORY,
        data: id,
      });
    });
  };
};

export const putCategory = (id, category, callback) => {
  axios.put(`${BASE_API}/category/${id}`, category).then((res) => {
    callback(res);
  });
};
