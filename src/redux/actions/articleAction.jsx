import * as actions from "./actionType";
import Axios from "axios";
import { BASE_API } from "../../api/BaseApi";

export const getArticles = () => {
  console.log(actions.GET_ARTICLE);
  return async (dp) => {
    const result = await Axios.get(`${BASE_API}/articles?page=1&size=10`);
    dp({
      type: actions.GET_ARTICLE,
      data: result.data,
    });
  };
};

export const getArticleById = (id) => {
  console.log(actions.GET_ARTICLE_BY_ID);
  return async (dp) => {
    const result = await Axios.get(`${BASE_API}/articles/${id}`);
    dp({
      type: actions.GET_ARTICLE_BY_ID,
      data: result.data,
      id: id,
    });
  };
};

export const deleteArticle = (id) => {
  console.log(actions.DELETE_ARTICLE);
  return async (dp) => {
    const result = await Axios.delete(`${BASE_API}/articles/${id}`);
    dp({
      type: actions.DELETE_ARTICLE,
      data: result.data,
      id: id,
    });
  };
};

export const postArticle = (
  title1,
  category1,
  description1,
  thumbnail1,
  publish1
) => {
  console.log(actions.POST_ARTICLE);
  return async (dp) => {
    const result = await Axios.post(`${BASE_API}/articles`, {
      title: title1,
      description: description1,
      published: publish1,
      image: thumbnail1,
      category: {
        _id: category1,
      },
    });
    dp({
      type: actions.POST_ARTICLE,
      data: result.data,
    });
  };
};

export const putArticle = (
  id,
  title1,
  category1,
  description1,
  thumbnail1,
  publish1
) => {
  console.log(actions.PUT_ARTICLE);
  return async (dp) => {
    const result = await Axios.patch(`${BASE_API}/articles/${id}`, {
      title: title1,
      description: description1,
      published: publish1,
      image: thumbnail1,
      category: {
        _id: category1,
      },
    });
    dp({
      type: actions.POST_ARTICLE,
      data: result.data,
      id: id,
    });
  };
};

export const uploadPicture = (file, callBack) => {
  Axios.post(`${BASE_API}/images`, file).then((res) => {
    callBack(res.data.url);
  });
};

export const getArticleByIdNoAwait = (id, callBack) => {
  Axios.get(`${BASE_API}/articles/${id}`).then((res) => {
    callBack(res.data.data);
  });
};

export const postArticleNoAwait = (article, callBack) => {
  Axios.post(`${BASE_API}/articles`, article).then((res) => {
    callBack(res.data.message);
  });
};
export const putArticleNoAwait = (article, Id, callBack) => {
  Axios.patch(`${BASE_API}/articles/${Id}`, article).then((res) => {
    callBack(res.data.message);
  });
};

export const searchArticleNoAwait = (title) => {
  return (dp) => {
    Axios.get(`${BASE_API}/articles?title=${title}`).then((res) => {
      dp({
        type: actions.SEARCH_ARTICLE_NO_AWAIT,
        data: res.data.data,
      });
    });
  };
};

export const getMoreArticlesNoAwait = (page) => {
  return (dp) => {
    Axios.get(`${BASE_API}/articles?page=${page}&size=10`).then((res) => {
      dp({
        type: actions.GET_MORE_ARTICLES_NO_AWAIT,
        data: res.data,
      });
    });
  };
};
