import React, { Component } from "react";
import { connect } from "react-redux";
import "./main.css";

import {
  getArticles,
  getArticleById,
  searchArticleNoAwait,
  getMoreArticlesNoAwait,
} from "../redux/actions/articleAction";
import { bindActionCreators } from "redux";
import {
  Navbar,
  Nav,
  NavDropdown,
  FormControl,
  Form,
  Button,
} from "react-bootstrap";
import HomeList from "./HomeList";
import { Link } from "react-router-dom";
import store from "../redux/store/store";
import strings from "../localization/languages";
import ScrollMoreArticles from "./ScrollMoreArticles";

import { BoxLoading } from "react-loadingg";

class Home extends Component {
  constructor() {
    super();
    this.state = {
      page: 1,
    };
    this.moreArticles = this.moreArticles.bind(this);
  }
  componentWillMount() {
    this.props.getArticles();
  }

  moreArticles = () => {
    this.setState({
      page: this.state.page + 1,
    });
    this.props.getMoreArticlesNoAwait(this.state.page);
  };

  render() {
    const articles = this.props.data.map((a) => {
      return a ? (
        <HomeList data={a} isView={false} key={this.props.data._id} />
      ) : (
        <BoxLoading />
      );
    });

    return (
      <div className="container">
        <Navbar>
          <Navbar.Brand as={Link} to="/">
            <h1>{strings.articleM}</h1>
          </Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
            <Form inline>
              <FormControl
                type="text"
                placeholder="Search"
                className="mr-sm-2"
                onChange={(e) =>
                  this.props.searchArticleNoAwait(e.target.value)
                }
              />
              <Button variant="outline-success">{strings.search}</Button>
            </Form>
            <Link to="/addArticle">
              <Button className="btn btn-primary btnAddMargin">
                {strings.add}
              </Button>
            </Link>
          </Navbar.Collapse>
        </Navbar>
        <div> {articles}</div>
        <br />
        <div className="text-center ">
          {" "}
          <button className="btn btn-info " onClick={this.moreArticles}>
            {" "}
            more articles{" "}
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.articleReducer.data,
  };
};

const mapDispatchToprops = (dispatch) => {
  return bindActionCreators(
    {
      getArticles,
      getArticleById,
      searchArticleNoAwait,
      getMoreArticlesNoAwait,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToprops)(Home);
