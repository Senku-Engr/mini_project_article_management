import React, { Component } from "react";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getArticles, getArticleById } from "../redux/actions/articleAction";
import HomeList from "./HomeList";
class View extends Component {
  componentDidMount() {
    this.props.getArticleById(this.props.match.params.id);
    console.log(this.props.articleById);
  }
  render() {
    return (
      <div>
        {this.props.articleById === null ? null : (
          <HomeList data={this.props.articleById} isView={true} />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    articleById: state.articleReducer.articleById,
  };
};

const mapDispatchToprops = (dispatch) => {
  return bindActionCreators(
    {
      getArticles,
      getArticleById,
    },
    dispatch
  );
};

export default withRouter(connect(mapStateToProps, mapDispatchToprops)(View));
