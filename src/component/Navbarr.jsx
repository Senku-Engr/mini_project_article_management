import React, { Component } from "react";
import {
  Navbar,
  Nav,
  NavDropdown,
  FormControl,
  Form,
  Button,
} from "react-bootstrap";
import "./main.css";
import strings from "../localization/languages";
import { Link } from "react-router-dom";

export default class Navbarr extends Component {
  render() {
    return (
      <div className="container">
        <Navbar bg="light" expand="lg">
          <Navbar.Brand as={Link} to="/">
            {strings.articleM}
          </Navbar.Brand>

          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link as={Link} to="/">
                {strings.article}
              </Nav.Link>
              <Nav.Link as={Link} to="/category">
                {strings.category}{" "}
              </Nav.Link>
              <NavDropdown title={strings.lang} id="basic-nav-dropdown">
                <NavDropdown.Item
                  onClick={() => {
                    console.log("to english");
                    this.props.changeLanguage("en");
                  }}
                >
                  {strings.en}
                </NavDropdown.Item>
                <NavDropdown.Item
                  onClick={() => {
                    console.log("to khmer");
                    this.props.changeLanguage("kh");
                  }}
                >
                  {strings.kh}
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}
