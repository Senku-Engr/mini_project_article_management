import React, { Component } from "react";
import { Table, Button, Form } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import strings from "../localization/languages";
import {
  getCategory,
  deleteCategory,
  postCategory,
  putCategory,
} from "../redux/actions/categoryAction";
class Category extends Component {
  constructor() {
    super();
    this.state = {
      isUpdate: false,
      updateId: "",
      nameError: "",
      name: "",
    };
  }

  componentWillMount() {
    this.props.getCategory();
    console.log(this.props.category);
  }

  handleChange = (e) => {
    this.setState({
      name: e.target.value,
    });
  };

  validate = () => {
    let noname = "";
    if (this.state.name === "") {
      noname = "* Category name can not be blank";
    }
    if (noname !== "") {
      this.setState({
        nameError: noname,
      });
      return false;
    }
    return true;
  };

  handleAddUpdate = () => {
    if (this.validate() === true) {
      const category = {
        name: this.state.name,
      };
      if (this.state.isUpdate === true) {
        putCategory(this.state.updateId, category, (res) => {
          this.props.getCategory();
          alert(res.data.message);
          this.setState({
            isUpdate: false,
            name: "",
            nameError: "",
          });
        });
      } else {
        postCategory(category, (res) => {
          this.props.getCategory();
          alert(res.data.message);
          this.setState({
            name: "",
            nameError: "",
          });
        });
      }
    }
  };
  handleEdit = (id, name) => {
    this.setState({
      isUpdate: true,
      name: name,
      updateId: id,
    });
  };

  render() {
    const categories = this.props.category.map((category, index) => (
      <tr key={category._id}>
        <td>{index + 1}</td>
        <td>{category.name}</td>
        <td>
          <Button
            variant="primary"
            className="mx-1"
            size="md"
            onClick={() => this.handleEdit(category._id, category.name)}
          >
            {strings.edit}
          </Button>

          <Button
            variant="danger"
            className="mx-1"
            size="md"
            onClick={() => this.props.deleteCategory(category._id)}
            disabled={this.state.isUpdate ? true : false}
          >
            {strings.delete}
          </Button>
        </td>
      </tr>
    ));
    return (
      <div>
        <div className="container">
          <h1 className="my-4">{strings.category}</h1>
          <Form.Group controlId="Name">
            <Form.Label>{strings.name}</Form.Label>
            <div className="form-inline w-75">
              <Form.Control
                type="text"
                placeholder="Input Category Name"
                name="name"
                value={this.state.name}
                onChange={this.handleChange}
              />

              <Button
                variant="dark"
                className="mx-1"
                size="md"
                onClick={this.handleAddUpdate}
              >
                {this.state.isUpdate ? "Save" : "Add Category"}
              </Button>
            </div>
            <Form.Text className="text-danger">
              {this.state.nameError}
            </Form.Text>
          </Form.Group>

          <Table striped bordered hover className="text-center">
            <thead>
              <tr>
                <th>#</th>
                <th>{strings.name}</th>
                <th>{strings.action}</th>
              </tr>
            </thead>
            <tbody>{categories}</tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    category: state.categoryReducer.category,
  };
};

const mapDispatchToprops = (dispatch) => {
  return bindActionCreators(
    {
      getCategory,
      deleteCategory,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToprops)(Category);
