import React from "react";
import { BoxLoading } from "react-loadingg";
import InfiniteScroll from "react-infinite-scroll-component";
function ScrollMoreArticles(props) {
  return (
    <InfiniteScroll
      dataLength={props.articles.length} //This is important field to render the next data
      next={props.getMoreArticle}
      hasMore={props.hasMore}
      loader={<BoxLoading />}
      endMessage={
        <p style={{ textAlign: "center" }}>
          <b>Yay! You have seen it all</b>
        </p>
      }
    >
      {props.articles}
    </InfiniteScroll>
  );
}

export default ScrollMoreArticles;
