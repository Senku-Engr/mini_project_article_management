import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import "./main.css";
import {
  getArticles,
  getArticleById,
  deleteArticle,
} from "../redux/actions/articleAction";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router";
import strings from "../localization/languages";

class Btn extends Component {
  constructor() {
    super();
    this.deleteArticle = this.deleteArticle.bind(this);
  }
  deleteArticle = (id) => {
    this.props.deleteArticle(this.props.data._id);
  };
  render() {
    return (
      <div>
        <Link to={`/view/${this.props.data._id}`}>
          <button className="btn btn-primary">{strings.view}</button>
        </Link>
        <Link to={`/addArticle?update=true&&id=${this.props.data._id}`}>
          <button className="btn btn-warning btnEditmargin">
            {strings.edit}
          </button>
        </Link>
        <button className="btn btn-danger " onClick={this.deleteArticle}>
          {strings.delete}
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    // data: state.articleReducer.data,
  };
};

const mapDispatchToprops = (dispatch) => {
  return bindActionCreators(
    {
      // getArticles,
      // getArticleById,

      deleteArticle,
    },
    dispatch
  );
};

export default withRouter(connect(mapStateToProps, mapDispatchToprops)(Btn));
